---
layout: '../../layouts/recipe.astro'
title: Ramen
meal_type: Dinner
course: Roast
diet: Healthy
main_ingredient: Meat and chicken
date: 2019-09-05
image: /images/classic-roast-chicken.jpg
---

# Markdown flavored recipe content

* ingredient 1
* ingredient 2