---
layout: '../../layouts/recipe.astro'
title: Carbonara
meal_type: Lunch
course: Roast
diet: Healthy
main_ingredient: Meat and chicken
date: 2020-08-05
image: /images/classic-roast-chicken.jpg
---

# Markdown flavored recipe content

* ingredient 1
* ingredient 2